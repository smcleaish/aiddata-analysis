# AidData Analysis

This project seeks to visualize the data presented in William and Mary's
AidData V3 dataset hosted at [aiddata.org](http://aiddata.org) using 
network analysis and mapping tools.  

From the AidData website:

### Summary

The dataset captures 20,985 projects across 165 low- and middle-income 
countries supported by loans and grants from official sector 
institutions in China worth $1.34 trillion. It tracks projects over 22 
commitment years (2000-2021) and provides details on the timing of 
project implementation over a 24-year period (2000-2023).

AidData. 2023. Global Chinese Development Finance Dataset, Version 3.0. 
Retrieved from https://www.aiddata.org/data/aiddatas-global-chinese-development-finance-dataset-version-3-0

## Data

The information was compiled into CSVs based on region. Clone this
repository and navigate to 
aiddata-analysis/csv-parsing/csv_parsing/data/processed
and run the following command to decompress the data:

```bash
tar -xjvf gcdf_files.tar.bz2
```
Inside you'll find a 3 files per region: 

* a CSV for all projects that were in the dataset that had a 
corresponding location entry suitable for mapping. 

* a node CSV that contains all the unique entities associated with the
listed projects

* an edge CSV that contains edges from the projects defining a directed
graph as follows:

| Source                          | Target                        |
|---------------------------------|-------------------------------|
| Funding Agencies                | Direct Receiving Agencies     |
| Funding Agencies                | Indirect Receiving Agencies   |
| Co-financing Agencies           | Direct Receiving Agencies     |
| Co-financing Agencies           | Indirect Receiving Agencies   |
| Insurance Provider              | Direct Receiving Agencies     |
| Insurance Provider              | Indirect Receiving Agencies   |
| Insurance Provider              | Funding Agencies              |
| Insurance Provider              | Co-financing Agencies         |
| Security Agent/Collateral Agent | Funding Agencies              |
| Security Agent/Collateral Agent | Co-financing Agencies         |
| Implementing Agencies           | Direct Receiving Agencies     |
| Implementing Agencies           | Indirect Receiving Agencies   |

As well as map, node, and edge files for the entire dataset.

Defining the relationships most appropriately is still a work in 
progress, and I welcome suggestions on how to improve the data.

The nodes and edges CSVs are suitable for use in network analysis tools
like Gephi.


## Parser

To run the parser code, clone this repository and install poetry:

```bash
pip install poetry
```

Then, navigate to the `aiddata-analysis/csv-parsing/` 
directory and run the following command:

```bash
poetry install
poetry shell
```
And then run the notebook `csv_parsing/csv_parsing.ipynb` 
with a Jupyter interpreter.

