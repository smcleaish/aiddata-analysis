# AidData CSV parser

Jupyter notebook to parse the AidData dataset. 

## Parser

To run the parser code, clone this repository and install poetry:

```bash
pip install poetry
```

Then, navigate to the `parser` directory and run the following command:

```bash
poetry install
poetry shell
```
And then run the notebook with a Jupyter interpreter.

